# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 21000101000000) do

  create_table "asset_images", :force => true do |t|
    t.string   "type"
    t.string   "location"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "asset_instances", :force => true do |t|
    t.integer  "x"
    t.integer  "y"
    t.integer  "asset_id"
    t.integer  "turn_id"
    t.integer  "asset_image_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "assets", :force => true do |t|
    t.string   "name"
    t.integer  "width"
    t.integer  "height"
    t.integer  "game_id"
    t.integer  "asset_image_id"
    t.integer  "prob_set_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "assets_players", :id => false, :force => true do |t|
    t.integer "asset_id"
    t.integer "player_id"
  end

  create_table "chat_items", :force => true do |t|
    t.integer  "player_id"
    t.text     "content"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "commitments", :id => false, :force => true do |t|
    t.integer "player_id"
    t.integer "turn_id"
  end

  create_table "events", :force => true do |t|
    t.integer  "player_id"
    t.text     "action",     :limit => 1048576
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "games", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "template",                                         :default => false
    t.integer  "boardwidth"
    t.integer  "boardheight"
    t.string   "boardimage"
    t.integer  "imagewidth"
    t.integer  "imageheight"
    t.boolean  "game_shared_visual",                               :default => false
    t.integer  "current_turn_id"
    t.integer  "default_terrain_id"
    t.decimal  "score",              :precision => 6, :scale => 4
    t.text     "score_components"
    t.datetime "started_at"
    t.datetime "finished_at"
    t.datetime "created_at",                                                          :null => false
    t.datetime "updated_at",                                                          :null => false
    t.boolean  "show_pre_score"
  end

  create_table "players", :force => true do |t|
    t.integer  "game_id"
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "prob_sets", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "probs", :force => true do |t|
    t.integer  "prob_set_id"
    t.integer  "asset_image_id"
    t.boolean  "target"
    t.decimal  "value",          :precision => 5, :scale => 4
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
  end

  create_table "roles", :force => true do |t|
    t.string   "name",              :limit => 40
    t.string   "authorizable_type", :limit => 40
    t.integer  "authorizable_id"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  create_table "roles_users", :id => false, :force => true do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "targets", :force => true do |t|
    t.string   "name"
    t.integer  "x"
    t.integer  "y"
    t.integer  "game_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "terrains", :force => true do |t|
    t.integer  "x"
    t.integer  "y"
    t.integer  "game_id"
    t.integer  "prob_set_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "turns", :force => true do |t|
    t.integer  "game_id"
    t.integer  "asset_image_id"
    t.string   "name"
    t.integer  "order_num"
    t.boolean  "shared_visual"
    t.boolean  "evaluated"
    t.boolean  "copy_assets_from_previous", :default => false
    t.datetime "started_at"
    t.datetime "finished_at"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "login"
    t.string   "email"
    t.string   "crypted_password",          :limit => 40
    t.string   "salt",                      :limit => 40
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "remember_token"
    t.datetime "remember_token_expires_at"
  end

end
