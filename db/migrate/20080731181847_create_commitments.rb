class CreateCommitments < ActiveRecord::Migration
  def self.up
    create_table :commitments, :id => false do |t|
      t.integer :player_id
      t.integer :turn_id
    end
  end

  def self.down
    drop_table :commitments
  end
end
