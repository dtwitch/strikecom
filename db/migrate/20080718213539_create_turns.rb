class CreateTurns < ActiveRecord::Migration
  def self.up
    create_table :turns do |t|
      t.references :game
      t.references :asset_image #the asset image
      t.string :name
      t.integer :order_num
      t.boolean :shared_visual
      t.boolean :evaluated
      t.boolean :copy_assets_from_previous, :default => false
      t.datetime :started_at
      t.datetime :finished_at
      t.timestamps
    end
  end

  def self.down
    drop_table :turns
  end
end
