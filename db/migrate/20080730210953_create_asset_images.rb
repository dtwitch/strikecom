class CreateAssetImages < ActiveRecord::Migration
  def self.up
    create_table :asset_images do |t|
      t.string :type
      t.string :location
      t.timestamps
    end
  end

  def self.down
    drop_table :asset_images
  end
end
