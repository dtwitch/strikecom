class CreateTerrains < ActiveRecord::Migration
  def self.up
    create_table :terrains do |t|
      t.integer :x
      t.integer :y
      t.references :game
      t.references :prob_set
      t.timestamps
    end
  end

  def self.down
    drop_table :terrains
  end
end
