class CreateChatItems < ActiveRecord::Migration
  def self.up
    create_table :chat_items do |t|
      t.references :player
      t.text :content
      t.timestamps
    end
  end

  def self.down
    drop_table :chat_items
  end
end
