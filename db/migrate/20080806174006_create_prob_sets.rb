class CreateProbSets < ActiveRecord::Migration
  def self.up
    create_table :prob_sets do |t|
      t.string  :name
      t.timestamps
    end
  end

  def self.down
    drop_table :prob_sets
  end
end
