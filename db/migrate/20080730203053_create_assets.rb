class CreateAssets < ActiveRecord::Migration
  def self.up
    create_table :assets do |t|
      t.string :name
      t.integer :width
      t.integer :height
      t.references :game
      t.references :asset_image
      t.references :prob_set
      t.timestamps
    end

    create_table :assets_players, :id => false do |t|
        t.references :asset
        t.references :player
    end
  end

  def self.down
    drop_table :assets_players
    drop_table :assets
  end
end
