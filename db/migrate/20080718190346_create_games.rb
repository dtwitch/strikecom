class CreateGames < ActiveRecord::Migration
  def self.up
    create_table :games do |t|
      t.string :name
      t.text  :description
      t.boolean :template, :default => false
      t.integer :boardwidth
      t.integer :boardheight
      t.string :boardimage
      t.integer :imagewidth
      t.integer :imageheight
      t.boolean :game_shared_visual, :default => false
      t.integer :current_turn_id
      t.integer :default_terrain_id
      t.decimal :score, :precision => 6, :scale => 4
      t.text    :score_components  #will hold serialized hash of score components
      t.datetime :started_at
      t.datetime :finished_at
      t.timestamps
    end
  end

  def self.down
    drop_table :games
  end
end
