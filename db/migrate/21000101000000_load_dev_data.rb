require 'active_record/fixtures'


#Images are actually at public/images.  they are in the database at strikecom/images so that the deployment will work correctly.
class LoadDevData < ActiveRecord::Migration
  def self.up
  
    down
    
    directory = File.join(File.dirname(__FILE__), "../../test/fixtures")
    ActiveRecord::Fixtures.create_fixtures(directory, "prob_sets")
    ActiveRecord::Fixtures.create_fixtures(directory, "probs")
    ActiveRecord::Fixtures.create_fixtures(directory, "asset_images")
    
    (1..1).each do |template_num|
      game = Game.new(:name => "Game #{template_num.to_s}",
               :boardwidth => 3,
               :boardheight => 3,
               :game_shared_visual => false,
               :score => 0,
               :template => true,
               :boardimage => '/assets/Mawsil2.jpg',
               :imagewidth => 400,
               :imageheight => 450)
      game.save!
      
      [{:x => 1, :y => 1},{:x => 2, :y => 2}].each do |target_hash|
        target = Target.new(target_hash)
        game.targets << target
        target.save!
      end

      default_terrain = Terrain.new(:x => 0, :y => 0, :prob_set => ProbSet.find_by_name('terrain_medium'))
      game.default_terrain = default_terrain
      game.save!
      game.terrains << default_terrain
      default_terrain.save!
      
      #normal turns
      num_turns = 3
      (1..num_turns).each do |turn_num|
        turn = Turn.new(:name => 'Turn ' + turn_num.to_s,
                        :order_num => turn_num,
                        :shared_visual => false,
                        :copy_assets_from_previous => true,
                        :evaluated => false)
        game.turns << turn
        turn.save!
      end
      
      #strike turn
      striketurn = Turn.new(:name => 'Strike Turn',
                            :order_num => num_turns + 1,
                            :shared_visual => true,
                            :copy_assets_from_previous => false,
                            :evaluated => false)
      game.turns << striketurn
      striketurn.save!
      #add the bombs to the striketurn
      bomb_image = AssetImage.new(:type => 'image',
                                     :location => '/assets/bomb.gif')
      bomb_image.save!

      #save the bomb assets for later binding to players
      bomb_assets = []
      
      #create enough bombs for all spaces on the board
      (0..(game.boardwidth * game.boardheight)).each do
        bomb_asset = Asset.new(:name => 'bomb',
                    :width => 1,
                    :height => 1,
                    :prob_set => ProbSet.find_by_name('asset_guarenteed'),
                    :asset_image => bomb_image,
                    :game => game)
        bomb_asset.save!
        bomb_assets << bomb_asset
        instance = AssetInstance.new(:x => 0, :y => 0)
        instance.turn = striketurn
        instance.asset = bomb_asset
        instance.save!
      end
      
      game.current_turn = Turn.find_by_name('Turn 1')
      game.save!
      
      players = {'air' => {'U2 Spy Plane' => 'u2.gif', 'F22' => 'f22.gif'}, 
                 'space' => {'Key Hole Hi-Res Satellite' => 'sattelite.gif', 'Eagle Satellite' => 'Optical_Satellite.gif'},
                 'intel' => {'HUMINT' => 'Spy.gif', 'SIGINT' => 'awacs.jpg'}}
                 
      players.each do |player_name, assets|
      
        player = Player.new(:name => player_name )
        game.players << player
        player.save!
        
        asset_sizes = {'asset_high' => 1, 'asset_medium' => 3}
        (0..1).each do |iter|
          asset_image = AssetImage.new(:type => 'image',
                                       :location => '/assets/' + assets[assets.keys[iter]])
          asset_image.save!
          asset = Asset.new(:name => assets.keys[iter],
                      :width => asset_sizes[asset_sizes.keys[iter]],
                      :height => 1,
                      :prob_set => ProbSet.find_by_name(asset_sizes.keys[iter]),
                      :asset_image => asset_image,
                      :game => game)
          asset.save!
          asset.players << player
          instance = AssetInstance.new(:x => 0, :y => 0)
          instance.asset = asset
          instance.turn = Turn.find_by_name("Turn 1", :conditions => {:game_id => game.id})
          instance.save!
        end #assets
        #tie the bomb assets to each player
        bomb_assets.each do |bomb_asset|
          bomb_asset.players << player
          bomb_asset.save
        end
      end #players
    end

    #create root
    root = User.new({:login => 'root', :password => 'pass',:password_confirmation => 'pass', :email => 'root@example.com'})
    root.save!
    
    #give root admin
    root.has_role 'admin'
    
	#Below method has been commented, it causes the migration to go into infinite loop
	Game.create_game_and_users(Game.find_by_name('Game 1'), 'test_game_1', '1')
  end

  def self.down
    Game.delete_all
    Player.delete_all
    User.delete_all
    Role.delete_all
    ChatItem.delete_all
    Turn.delete_all
    Asset.delete_all
    AssetInstance.delete_all
    Target.delete_all
    Terrain.delete_all
    AssetImage.delete_all
    Commitment.delete_all
    ProbSet.delete_all
    Prob.delete_all
  end
end

