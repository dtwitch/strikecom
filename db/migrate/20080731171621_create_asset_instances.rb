class CreateAssetInstances < ActiveRecord::Migration
  def self.up
    create_table :asset_instances do |t|
      t.integer :x
      t.integer :y
      t.references :asset
      t.references :turn
      t.references :asset_image
      t.timestamps
    end
  end

  def self.down
    drop_table :asset_instances
  end
end
