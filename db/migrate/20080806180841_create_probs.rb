class CreateProbs < ActiveRecord::Migration
  def self.up
    create_table :probs do |t|
      t.references :prob_set
      t.references :asset_image
      t.boolean :target
      t.decimal :value, :precision => 5, :scale => 4
      t.timestamps
    end
  end

  def self.down
    drop_table :probs
  end
end
