class AddShowPreScoreToGames < ActiveRecord::Migration
  def self.up
    add_column :games, :show_pre_score, :boolean
  end

  def self.down
    remove_column :games, :show_pre_score
  end
end
