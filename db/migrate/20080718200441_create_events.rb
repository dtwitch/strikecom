class CreateEvents < ActiveRecord::Migration
  def self.up
    create_table :events do |t|
      t.references :player
      t.text    :action, :limit => 1.megabyte
      t.timestamps
    end
  end

  def self.down
    drop_table :events
  end
end
