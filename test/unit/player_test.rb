require 'test_helper'

class PlayerTest < ActiveSupport::TestCase
  fixtures :games, :players, :asset_rights, :assets, :chat_items, :turns
  def test_commit
    player = players(:air1)
    assert_difference 'Commitment.count' do
      player.commit(turns(:turn_1_1))
    end
    assert player.committed?
  end
  
  def test_has_many_asset_rights
    assert_not_equal 0, players(:air1).asset_rights.length
  end

  def test_has_many_assets_through_asset_rights
    assert_not_equal 0, players(:air1).assets.length
  end

  def test_has_many_chat_items
    assert_not_equal 0, players(:air1).chat_items.length
  end
  
  def test_belongs_to_game
    assert_not_nil players(:air1).game
  end

end
