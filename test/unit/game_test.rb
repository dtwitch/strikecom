require 'test_helper'

class GameTest < ActiveSupport::TestCase
  fixtures :games, :players, :turns, :terrains, :targets, :chat_items
  # Replace this with your real tests.
  def test_invalid_with_empty_attributes
    game = Game.new
    assert !game.valid?, 'game valid when empty'
    assert game.errors.invalid?(:name)
    assert game.errors.invalid?(:boardwidth)
    assert game.errors.invalid?(:boardheight)
  end
 
  def test_current_turn
    game = games(:test_game_1)
    assert_equal(game.current_turn, turns(:turn_1_1))
  end
 
   #must be at least three chat items in fixtures
  def test_see_chat_items_in_order
    game = games(:test_game_1)
    assert game.chat_items.count > 0
    assert game.chat_items[1].created_at <= game.chat_items[2].created_at && 
              game.chat_items[2].created_at <= game.chat_items[-1].created_at
  end
  
  def test_has_many_players
    assert_not_equal 0, games(:test_game_1).players.length
  end
  
  def test_has_many_turns
    assert_not_equal 0, games(:test_game_1).turns.length
  end

  def test_has_many_terrains
    assert_not_equal 0, games(:test_game_1).terrains.length
  end
  
  def test_has_many_targets
    assert_not_equal 0, games(:test_game_1).targets.length
  end


end
