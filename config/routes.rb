StrikeComRuby::Application.routes.draw do
  
  namespace :admin  do 
    resources :game do
      member do
        get 'clone'
      end 
      collection do
        post 'create_many'
        get 'list_games'
        get 'list_templates'
        get 'list'
      end
      as_routes 
   end
   resources :user do
      as_routes
   end
   resources :player do
      as_routes
   end
   resources :turn do
      as_routes
   end
   resources :asset do
      as_routes
   end
   resources :target do
      as_routes
   end
   resources :asset_instance do
      as_routes
   end
   resources :asset_image do
      as_routes
   end
  end  
  root :to => 'game#index'

  
  #Authentication failures in admin should go to the root account controller
  
  match 'admin/account/login', :to=>'account#login'
  match 'admin/account/logout', :to=>'account#logout'
  
  
  namespace :chat do
    resources :chat_item
  end
  
  match ':controller(/:action(/:id))(.:format)'
  match '/:controller(/:action(/:id))'
  match '/:controller(/:action(/:id))(.:format)'
end
