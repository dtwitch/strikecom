
#This script will print the length of all finished games in csv format

games = Game.find(:all, :conditions => "finished_at IS NOT NULL")

print "game_name,length,chat_length,turns\n"
games.each do |game| 
  chats = game.chat_items.find(:all, :order => "created_at ASC")
  print game.name + "," + (game.finished_at - game.started_at).to_s + "," + ((chats.last ? chats.last.created_at : 0) - (chats.first ? chats.first.created_at : 0)).to_s + "," + chats.count.to_s + "\n"
end
