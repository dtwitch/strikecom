#this script lists all of the finished games along with 
#the locations of placed bombs and the targets


#find all the finished games
games = Game.find(:all, :conditions => "finished_at IS NOT NULL")

convert = {1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F'}

games.each do |game|
  puts game.name
  puts "\tplaced bombs:"
  game.turns[-1].asset_instances.each do |instance|
    puts "\t\t" + convert[instance.x] + "   " + instance.y.to_s unless instance.x == 0 and instance.y == 0
  end
  puts "\ttargets"
  game.targets.each do |target|
    puts "\t\t" + convert[target.x] + "   " + target.y.to_s
  end
end