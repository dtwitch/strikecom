#this script lists all of the finished games along with 
#the locations of placed bombs and the targets and calculates 
#the resulting game scores
#Output in csv format

#find all the finished games
require 'set'
games = Game.find(:all, :conditions => "finished_at IS NOT NULL")

convert = {0 => '0', 1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F'}

print "game_name,hits,misses,targets,score\n"
games.each do |game|
  target_set = Set.new
  bomb_set = Set.new
  print game.name + ","
  game.targets.each do |target|
    target_set.add(convert[target.x] + target.y.to_s)
  end
  #make sure that the bombs are unique.  No penalty for more than one bomb in one place
  game.turns[-1].asset_instances.each do |instance|
    bomb_set.add(convert[instance.x] + instance.y.to_s) unless instance.x == 0 and instance.y == 0
  end
  hits = 0
  misses = 0
  bomb_set.each do |bomb|
    if (target_set.member?(bomb))
      hits = hits + 1
    else
      misses = misses + 1
    end
  end
  print hits.to_s + "," + misses.to_s + "," + target_set.size.to_s + "," + (hits.to_f / (target_set.size.to_f + misses.to_f)).to_s + "\n"
end
