# == Schema Information
# Schema version: 21000101000000
#
# Table name: probs
#
#  id             :integer(11)     not null, primary key
#  prob_set_id    :integer(11)     
#  asset_image_id :integer(11)     
#  target         :boolean(1)      
#  value          :decimal(5, 4)   
#  created_at     :datetime        
#  updated_at     :datetime        
#

class Prob < ActiveRecord::Base
  #relationships
  belongs_to :prob_set
  belongs_to :asset_image #this is the image that the asset should display given this prob is selected
  
  #validations
  #validates_numericality_of :value, :greater_than_or_equal_to => 0,  :less_than_or_equal_to => 0, :message => 'must be a number between 0 and 1, inclusive'

  def to_label
    self.prob_set.name + ' ' + self.asset_image.location
  end
end
