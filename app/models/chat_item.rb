# == Schema Information
# Schema version: 21000101000000
#
# Table name: chat_items
#
#  id         :integer(11)     not null, primary key
#  player_id  :integer(11)     
#  content    :text            
#  created_at :datetime        
#  updated_at :datetime        
#

class ChatItem < ActiveRecord::Base
	attr_accessible :player_id, :content
	belongs_to  :player
	validates :player, :presence => true
end
