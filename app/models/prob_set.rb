# == Schema Information
# Schema version: 21000101000000
#
# Table name: prob_sets
#
#  id         :integer(11)     not null, primary key
#  name       :string(255)     
#  created_at :datetime        
#  updated_at :datetime        
#

class ProbSet < ActiveRecord::Base
  
  #relationships
  has_many :probs, :order => 'value ASC'
  has_many :terrains
  has_many :assets
  
  #validations
  validates_uniqueness_of :name
 
end
