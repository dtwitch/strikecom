# == Schema Information
# Schema version: 21000101000000
#
# Table name: targets
#
#  id         :integer(11)     not null, primary key
#  name       :string(255)     
#  x          :integer(11)     
#  y          :integer(11)     
#  game_id    :integer(11)     
#  created_at :datetime        
#  updated_at :datetime        
#

class Target < ActiveRecord::Base
  
  #attr_accessible for mass assignment rails 3.2
  attr_accessible :x, :y 
  
  #relationships
  belongs_to :game
  
  #validations
  validates_numericality_of :x, :y, :greater_than => 0, :only_integer => true, :message => 'must be an integer greater than 0'
  validates_existence_of :game
  
  def deep_clone(new_attributes={})
    new_target = self.dup
    new_target.created_at = Time.now
    new_target.update_attributes(new_attributes) #updates and saves
    new_target.save!
    new_target
  end  
  
  def to_label
    self.x.to_s + ',' + self.y.to_s
  end  
end
