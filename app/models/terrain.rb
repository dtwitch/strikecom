# == Schema Information
# Schema version: 21000101000000
#
# Table name: terrains
#
#  id          :integer(11)     not null, primary key
#  x           :integer(11)     
#  y           :integer(11)     
#  game_id     :integer(11)     
#  prob_set_id :integer(11)     
#  created_at  :datetime        
#  updated_at  :datetime        
#

class Terrain < ActiveRecord::Base
  
  #attr_accessible for mass assignment rails 3.2
  attr_accessible :x, :y, :prob_set
  
  #relationships
  belongs_to :game
  belongs_to :prob_set
  
  #validations
  validates_numericality_of :x, :y, :greater_than_or_equal_to => 0, :only_integer => true, :message => 'must be an integer greater than 0'
  validates_existence_of :game
  
  def deep_clone(new_attributes={})
    new_terrain = self.dup
    new_terrain.created_at = Time.now
    new_terrain.prob_set = self.prob_set
    new_terrain.update_attributes(new_attributes) #updates and saves
    new_terrain.save!
    new_terrain
  end
  
  def to_label
    self.x.to_s + ',' + self.y.to_s
  end
end
