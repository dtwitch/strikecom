# == Schema Information
# Schema version: 21000101000000
#
# Table name: commitments
#
#  player_id :integer(11)     
#  turn_id   :integer(11)     
#

class Commitment < ActiveRecord::Base
  
  #relationships
  belongs_to :player
  belongs_to :turn
  
  #validations
  validates_existence_of :player, :turn
end
