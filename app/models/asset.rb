# == Schema Information
# Schema version: 21000101000000
#
# Table name: assets
#
#  id             :integer(11)     not null, primary key
#  name           :string(255)     
#  width          :integer(11)     
#  height         :integer(11)     
#  game_id        :integer(11)     
#  asset_image_id :integer(11)     
#  prob_set_id    :integer(11)     
#  created_at     :datetime        
#  updated_at     :datetime        
#

class Asset < ActiveRecord::Base
  
  #attr_accessible for mass assignment rails 3.2
  attr_accessible :name, :width, :height, :prob_set, :asset_image, :game
  
  #relationships
  has_many :asset_instances, :dependent => :destroy
  belongs_to :game
  has_and_belongs_to_many :players
  belongs_to :prob_set
  belongs_to :asset_image
  
  #validations
  validates_numericality_of :width, :height, :greater_than => 0, :only_integer => true, :message => 'must be an integer greater than 0'
  validates_presence_of :name, :width, :height, :asset_image_id
  validates_existence_of :asset_image

  def deep_clone(game, new_attributes={})
    new_asset = self.dup
    new_asset.game = game
    new_asset.created_at = Time.now
    new_asset.prob_set = self.prob_set
    new_asset.asset_image = self.asset_image #unless self.asset_image.nil? #no deep_clone on asset_image needed
    new_asset.update_attributes(new_attributes) #updates and saves
    new_asset.save!
    self.asset_instances.each { |asset_instance| new_asset.asset_instances << asset_instance.deep_clone(game)}
    new_asset.save!
    new_asset
  end
  
  def to_label
    self.game.name + ' - ' + self.name
  end  
end
