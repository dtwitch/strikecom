# == Schema Information
# Schema version: 21000101000000
#
# Table name: asset_instances
#
#  id             :integer(11)     not null, primary key
#  x              :integer(11)     
#  y              :integer(11)     
#  asset_id       :integer(11)     
#  turn_id        :integer(11)     
#  asset_image_id :integer(11)     
#  created_at     :datetime        
#  updated_at     :datetime        
#

class AssetInstance < ActiveRecord::Base
  
  attr_accessible :x, :y, :turn, :asset
  
  #relationships
  belongs_to :turn
  belongs_to :asset
  belongs_to :asset_image
  
  #validations
  validates_numericality_of :x, :y, :greater_than_or_equal_to => -1, :only_iteger => true, :message => 'must be an integer greater than 0'
  validates_existence_of :turn, :asset
  
  def deep_clone(game, new_attributes={})
    new_asset_instance = self.dup
    new_asset_instance.created_at = Time.now
    new_asset_instance.update_attributes(new_attributes) #updates and saves
    #order numbers are unique to each game so we can use them to find the 
    #turn this asset_instance is to connect to
    new_asset_instance.turn = game.turns.find_by_order_num(self.turn.order_num)
    new_asset_instance.save!
    new_asset_instance
  end
  
  #returns the image that should be displayed for this instance.  
  #If it has an evaluated image, it will return that
  #otherwise, it will use the asset's image
  def image
    self.asset_image || self.asset.asset_image
  end
  
  def to_label
    self.asset.name + ' at ' + self.x.to_s + ',' + self.y.to_s 
  end
end
