# == Schema Information
# Schema version: 21000101000000
#
# Table name: events
#
#  id         :integer(11)     not null, primary key
#  player_id  :integer(11)     
#  action     :text            
#  created_at :datetime        
#  updated_at :datetime        
#


class Event < ActiveRecord::Base
  attr_accessible :player_id, :action

  belongs_to :player

  #Don't want orphan events
  validates_existence_of :player
  
  #put an event in the queue for the given player
  #an action is javascript
  def Event.enqueue(acting_player, action)
    transaction do
      create(:player_id => acting_player.id, :action => action)
    end
  end
  
  #get a player's events out of the queue
  #the events action can be retrieved with the 'action' method
  #returns the javascript, not the event object
  def Event.dequeue(player_id)
    players_events = []
    transaction do
      players_events = find(:all, :conditions => {:player_id => player_id}, :lock => 'FOR UPDATE')
      delete_all ["player_id = ?", player_id]
    end
    players_events.collect {|x| x.action }
  end
end
