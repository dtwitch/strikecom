# == Schema Information
# Schema version: 21000101000000
#
# Table name: games
#
#  id                 :integer(11)     not null, primary key
#  name               :string(255)     
#  description        :text            
#  template           :boolean(1)      
#  boardwidth         :integer(11)     
#  boardheight        :integer(11)     
#  boardimage         :string(255)     
#  imagewidth         :integer(11)     
#  imageheight        :integer(11)     
#  game_shared_visual :boolean(1)      
#  current_turn_id    :integer(11)     
#  default_terrain_id :integer(11)     
#  score              :decimal(6, 4)   
#  started_at         :datetime        
#  finished_at        :datetime        
#  created_at         :datetime        
#  updated_at         :datetime        
#

require "fastercsv"

class Game < ActiveRecord::Base
  
  #attr_accessible for mass assignment rails 3.2
  attr_accessible :name, :boardwidth, :boardheight, :game_shared_visual, :score, :template, :boardimage, :imagewidth, :imageheight
  
  #Relationships
  has_many :players, :dependent => :destroy
  has_many :turns, :dependent => :destroy, :order => 'order_num ASC'
  has_many :terrains, :dependent => :destroy
  has_many :targets, :dependent => :destroy
  has_many :assets, :dependent => :destroy
  
  #Allows game to show all of the chats created by all of the players in the game
  has_many :chat_items, :through => :players, :order => 'chat_items.created_at'
  
  #refers back to turn as the current turn
  belongs_to :current_turn, :class_name => 'Turn', :foreign_key => 'current_turn_id'
 
  #refers to the default terrain
  belongs_to :default_terrain, :class_name => 'Terrain', :foreign_key => 'default_terrain_id'
  
  #validations
  validates_presence_of :name, :boardwidth, :boardheight
  validates_numericality_of :boardwidth, :boardheight, :only_integer => true, :greater_than => 0
  
  #named scopes
  #named_scope :not_finished, :conditions => {:finished_at => nil}
  scope :not_finished, :conditions => {:finished_at => nil}
  
  #Score components is a hash of hits targets and assets
  serialize :score_components
  
  #gets all of the other players in the game
  def other_players(the_player)
    Player.find(:all, :conditions => ["game_id = ? and id <> ?" , id, the_player.id])
  end

  #returns the turns that are finished along with the current turn.
  def finished_and_current_turns
    (self.turns.select {|turn| turn.finished_at})  << self.current_turn
  end
  
  def advance_turn
    transaction do
      current_turn_index = self.turns.index(self.current_turn)
      if(current_turn_index < self.turns.size - 1)
        next_turn = self.turns[current_turn_index + 1]
        #clone the turn's asset instances and put them in the next one if required
        if(next_turn.copy_assets_from_previous)
          self.current_turn.asset_instances.each do |instance|
            new_instance = instance.dup
            new_instance.asset_image_id = nil
            new_instance.turn = next_turn
            new_instance.save
            next_turn.asset_instances << new_instance
          end
        end
        #set the current turn to the next one
        self.current_turn = next_turn
      else #in this case the game is done
        self.finish_game
      end
      save
    end
  end
  
  #deep clones this game 
  #new_attributes parameter contains any attributes that should be changed.
  #saves the clone 
  def deep_clone(new_attributes={})
	logger.info "*********DEEP_CLONE*************"
    new_game = nil
    Game.transaction do
      new_game = self.dup;
      new_game.created_at = Time.now
      new_game.save!
      new_game.update_attributes(new_attributes) #update and saves
	  logger.info "*********BEFORE TURNS*************"
      self.turns.each { |turn| new_game.turns << turn.deep_clone}
	  logger.info "*********AFTER TURNS*************"
      new_game.current_turn = Turn.find(:first, :conditions => {:name => self.current_turn.name, :game_id => new_game.id})
	  logger.info "*********BEFORE TERRAIN*************"
      self.terrains.each { |terrain| new_game.terrains << terrain.deep_clone}
	  logger.info "*********AFTER TERRAIN*************"
      new_game.default_terrain = Terrain.find(:first, :conditions => {:x => self.default_terrain.x, :y => self.default_terrain.y, :game_id => new_game.id})
	  logger.info "*********BEFORE TARGET*************"
      self.targets.each { |target| new_game.targets << target.deep_clone}
	  logger.info "*********AFTER TARGET*************"
      new_game.save!
      #Players have to be done after the turns so that the asset instances can be connected to the correct turn
      old_assets_new_players = {}
      self.players.each do |player|
        #add players associated with each asset to a hash keyed on unique assets to give us a list of 
        #unique old assets that we will clone and with which we will have a list of new players to associate with
        new_player = player.deep_clone(new_game)
        new_game.players << new_player
        player.assets.each {|asset| (old_assets_new_players[asset] ||= []) << new_player } 
        player.save
      end
      #clone the unique assets and associate them with their players.
      old_assets_new_players.each do |asset, player_list|
        new_asset = asset.deep_clone(new_game)
        player_list.each {|player| new_asset.players << player }
        new_asset.save
      end
      new_game.save!
    end  
    return new_game
  end
  
  #create a game based on the given template.  Use the name for the new game
  #use the user_suffix plus the template's player names to create associated users
  def self.create_game_and_users(template, name, user_suffix)
  logger.info "*********CREATE_GAME_AND_USER*************"
    game = nil
    Game.transaction do 
      game = create_game(template, name)
      game.template = false
      game.save!
      game.players.each do |player|
        u = User.find(:first, {:conditions => {:login => player.name + user_suffix}})
        if (!u)
          u = User.new :login => player.name + user_suffix, :password => user_suffix, :password_confirmation => user_suffix, :email => player.name + "@" + name
          u.save!
        end
		#u.has_role 'owner'
        u.has_role('owner', player)##needs to be fixed
      end
    end
    game
  end

  #create a game with the given name based on the given template 
  def self.create_game(template, name)
  logger.info "*********CREATE_GAME*************"
    Game.transaction do
      new_game = template.deep_clone
      new_game.name = name
      new_game.save!
      new_game
    end
  end
  
  def to_csv(player=nil)
     logger.debug("getting chat for player: " + player.name) if player
      csv_string = FasterCSV.generate do |csv|
        # header row
        csv << ["time", "player", "content"]

        # data rows
        conditions = {}
        conditions[:player_id] = player.id if player
       self.chat_items.find(:all, :conditions => conditions, :order => "created_at ASC", :include => :player).each do |chat|
          csv << [chat.created_at, chat.player.name, chat.content]
       end
     end
    return csv_string
  end
  
  #called when the game is considered finished
  #sets the finished time and saves the score
  def finish_game
      self.finished_at = Time.now
  end
  
  #calculates and returns the score for the game
  def Game.calculate_score(hits, targets, assets)
    hits.to_f / ([targets.to_f, assets.to_f].max)
  end
end 
