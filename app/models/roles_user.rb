class RolesUser < ActiveRecord::Base
  attr_accessible :role_id, :user_id
  belongs_to :user
  belongs_to :role
  
  self.primary_key = :user_id
  
end
