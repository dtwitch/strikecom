# == Schema Information
# Schema version: 21000101000000
#
# Table name: players
#
#  id         :integer(11)     not null, primary key
#  game_id    :integer(11)     
#  name       :string(255)     
#  created_at :datetime        
#  updated_at :datetime        
#

class Player < ActiveRecord::Base

  #attr_accessible for mass assignment rails 3.2
  attr_accessible :name

  #is authorizable through the authorization plugin
  #users can be assigned rights to players
  acts_as_authorizable
  
  #relationships
  belongs_to  :game
  has_many    :chat_items, :dependent => :destroy
  has_and_belongs_to_many    :assets
  has_many    :commitments
  has_and_belongs_to_many :committed_turns,  :join_table => :commitments, :class_name => 'Turn'

  #validations
  validates_presence_of :name
  validates_existence_of :game
    
  #returns true if this player has committed all its assets' placements for the given turn
  def committed?(turn)
    if turn.kind_of? Fixnum
      return self.commitments.any? {|commit| commit.turn_id == turn}
    elsif turn.kind_of? Turn
      return self.commitments.any? {|commit| commit.turn_id == turn.id} 
    else
      logger.debug('returning false by default in committed? turn is a ' + turn.class.to_s)
      return false
    end
  end

  def my_instance?(instance)
    instance.asset.players.any? {|p| p.id == self.id} 
  end

  #commits this player's asset placement for the current turn
  def commit_current_turn
    self.commit(self.game.current_turn)
  end
  
  #commits this player's asset placement for the current turn
  def commit(turn)
    self.committed_turns << turn unless self.committed_turns.member?(turn)
  end
  
  def deep_clone(game, new_attributes={})
    new_player = self.dup
    new_player.created_at = Time.now
    #assets are now cloned at the game level
    #self.assets.each { |asset| new_player.assets << asset.deep_clone(game)}
    new_player.update_attributes(new_attributes) #updates and saves
    new_player.save!
    new_player
  end
end
