# == Schema Information
# Schema version: 21000101000000
#
# Table name: asset_images
#
#  id         :integer(11)     not null, primary key
#  type       :string(255)     
#  location   :string(255)     
#  created_at :datetime        
#  updated_at :datetime        
#

class AssetImage < ActiveRecord::Base
  attr_accessible :location, :type
  AssetImage.inheritance_column = :itype
  #relationships
  has_one :asset

  #validations
  validates_presence_of :type, :location

  def to_label
    return location
  end
end
