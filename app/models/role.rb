# == Schema Information
# Schema version: 21000101000000
#
# Table name: roles
#
#  id                :integer(11)     not null, primary key
#  name              :string(40)      
#  authorizable_type :string(40)      
#  authorizable_id   :integer(11)     
#  created_at        :datetime        
#  updated_at        :datetime        
#

# Defines named roles for users that may be applied to
# objects in a polymorphic fashion. For example, you could create a role
# "moderator" for an instance of a model (i.e., an object), a model class,
# or without any specification at all.
class Role < ActiveRecord::Base

  #attr_accessible for mass assignment rails 3.2
  attr_accessible :name, :authorizable
  
  #has_and_belongs_to_many :users
  #has_and_belongs_to_many :users, :join_table => :roles_users
  has_many :roles_user
  has_many :users, :through => :roles_user
  
  belongs_to :authorizable, :polymorphic => true
end
