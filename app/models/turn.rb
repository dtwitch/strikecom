# == Schema Information
# Schema version: 21000101000000
#
# Table name: turns
#
#  id                        :integer(11)     not null, primary key
#  game_id                   :integer(11)     
#  name                      :string(255)     
#  order_num                 :integer(11)     
#  evaluated                 :boolean(1)      
#  copy_assets_from_previous :boolean(1)      
#  started_at                :datetime        
#  finished_at               :datetime        
#  created_at                :datetime        
#  updated_at                :datetime        
#

class Turn < ActiveRecord::Base
  #attr_accessible for mass assignment rails 3.2
  attr_accessible :name, :order_num, :shared_visual, :copy_assets_from_previous, :evaluated
  
  #relationships
  belongs_to :game
  has_many :asset_instances
  has_and_belongs_to_many :committed_players,  :join_table => :commitments, :class_name => 'Player'
  
  #validations
  validates_existence_of :game
  validates_presence_of :name
  
  #returns true if all players for the given turn have committed
  def all_committed?
    self.game.players.sort { |a,b| a.id <=> b.id} == self.committed_players.sort { |a,b| a.id <=> b.id}
  end

  #will evaluate this turn if not already evaluated.  
  def evaluate
    evaluated = false
    #we need this to be consistent, so we'll lock the current turn at this point
    transaction do
      @game = Game.find(:first, :conditions => {:current_turn_id => self.id}, :include => [{:terrains => {:prob_set => :probs}}, :targets, :default_terrain])
      #Lock this turn so that no one else can see it until the evaluation is either done or failed
      self.lock!(:lock => 'FOR UPDATE')
      #If the all the players in the turn have committed and we're not evaluated
      if self.all_committed? & !self.evaluated
        terrains = @game.terrains
        default_terrain = @game.default_terrain
        targets = @game.targets
        #keep track of the hits for scoring
        hits = 0
        #eager load instance information.  Don't include off-board assets
        instances = self.asset_instances.find(:all, :conditions => ["x > 0 AND y > 0"], :include => {:asset => [:asset_image, {:prob_set => :probs}]})
        instances.each do |instance|        
          #get the terrain associated with this instance (or the default terrain)          
          terrain = terrains.detect{ |t| t.x == instance.x && t.y == instance.y} || default_terrain
          target = targets.any?{ |t| t.x == instance.x && t.y == instance.y}
          hits = hits + 1 if target #add to the hits if a target is present at the position of the asset
          r = rand
          logger.debug('random number is ' + r.to_s)
          #combine the instance and terrain probabilities
          probs = combine_probs(instance, terrain, target)
          sorted_keys = probs.keys.sort
          #get the response for the probability for which the random number is less than, but greater than
          #the previous probability and assign its image to the instances asset
          matched_prob = probs[sorted_keys.detect {|key| r <= key }]
          #TEMPORARY FIX
          if(matched_prob)
            instance.asset_image = matched_prob.asset_image
          else
            instance.asset_image = AssetImage.create(:type => 'asset', :location => '/images/zero.gif')
            logger.debug('defaulting to zero since matched_prob was null')
          end  
          logger.debug('asset_image therefore is ' + instance.asset_image.location)
          instance.save
        end #each instance
        self.evaluated = true
        self.finished_at = Time.now
        self.save
        #If this is a non-copy turn (the last one) save the score to the game
       if(!self.copy_assets_from_previous)
         @game.score_components = {'hits' => hits, 'targets' => targets.length, 'assets' => instances.length}
         @game.score = Game.calculate_score(hits, targets.length, instances.length)
         #don't need to save here because @game.advance_turn below will do it for us
       end 
        @game.advance_turn
        evaluated = true
      end #if all committed
    end #transaction
    evaluated
  end
  
  def board_asset_instances(player)
    turn_instances = AssetInstance.find(:all, :conditions => {:turn_id => self.id}, :include => [:asset_image, :turn, {:asset => [:asset_image, {:players => :commitments}]}])

    #if not shared visual then remove the instances of other players
   if !self.shared_visual
     turn_instances = turn_instances.find_all {|instance| player.my_instance?(instance)}     
   end
    #put these in a hash according to their x and y. 
    #This will be used in the board
    asset_instances = {}
    turn_instances.each do |instance|
      (asset_instances[instance.x.to_s + ',' + instance.y.to_s] ||= []) << instance
    end
    asset_instances
  end

  def deep_clone(new_attributes={})
    new_turn = self.dup
    new_turn.created_at = Time.now
    new_turn.update_attributes(new_attributes) #updates and saves
    new_turn.save!
    new_turn
  end
  
  private 
   #combines the probabilities from a terrain and an asset and whether or not there is a target at the given terrain
   #Returns a hash that has the combined probabilities as the key and the the response as the value 
   ##### right now it just returns the asset probability
   def combine_probs (instance, terrain, target)
     probs = {}
     cumulative = 0
     asset_probs = instance.asset.prob_set.probs.select {|prob| prob.target == target} 
     asset_probs.each do |a_prob|
       #logger.debug('for instance at ' + instance.x.to_s + ',' + instance.y.to_s + 'before combination: ' + a_prob.asset_image.location + ' target: ' + target.to_s + ' asset prob set name' + a_prob.prob_set.name)
       #t_prob = terrain.prob_set.probs.detect { |t| t.asset_image == a_prob.asset_image && t.target == target }
       #logger.debug('terrain_probability = ' + t_prob.value.to_s + ' asset_probability = ' + a_prob.value.to_s + ' for image ' + a_prob.asset_image.location + ' and target = ' + target.to_s)
       #cumulative += (t_prob.value + a_prob.value) / 2
       cumulative += a_prob.value
       probs[cumulative] = a_prob 
       logger.debug('for instance at ' + instance.x.to_s + ',' + instance.y.to_s + ' probability threshold of ' + a_prob.asset_image.location + ' is ' + cumulative.to_s)
     end
     probs
   end
end
