module GameHelper
  
  #returns the name of the view's cell given the name of the instance
  def get_instance_name(asset_instance)
    'turn_' + asset_instance.turn_id.to_s + '_instance_' + asset_instance.asset.id.to_s + '_' + asset_instance.x.to_s + '_' + asset_instance.y.to_s
  end
  
  def get_cell_name(turn_id, x,y)
    return 'turn_' + turn_id.to_s + 'cell_' + x.to_s + '_' + y.to_s
  end
  
  def instance_committed?(instance)
    return instance.asset.players.all? {|player| player.committed?(instance.turn) }
  end
end
