module ApplicationHelper
  def broadcast(channel, &block)
    message = {:channel => channel, :data => capture(&block)}
    uri = URI.parse(FAYE_CONFIG[:server])
    Net::HTTP.post_form(uri, :message => message.to_json)
    logger.debug("broadcasting #{capture(&block)} to #{channel}")
  end
end
