// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults

var diplayed_turn = 0;

function display_turn(turn_id){
	diplayed_turn = turn_id;
	//remove the shown_turn class from all other turns
	//$$('.turn').each(function(turn){
	//	turn.removeClassName('shown_turn');
	//});
	$j(".turn").each(function(i, turn) {
	    console.log("removing show_turn from" + turn);
		$j(this).removeClass('shown_turn');
	});
	
	
	//set the turn navs of other turns to not shown
	//$$('.turn_nav').each(function(turn){
	//	turn.removeClassName('shown_turn_nav');
	//});
	$j(".turn_nav").each(function(i, turn) {
        console.log("removing show_turn_nav from" + turn);
		$j(this).removeClass('shown_turn_nav');
	});
	
	//set display of selected turn and its turn_nav to shown
	//$('turn_' + turn_id).addClassName('shown_turn');
	$j("#turn_" + turn_id).addClass('shown_turn');
    console.log("add show_turn to " + $j("#turn_" + turn_id))
	
	//$('turn_nav_' + turn_id).addClassName('shown_turn_nav');
	$j("#turn_nav_" + turn_id).addClass('shown_turn_nav');
    console.log("add show_turn_nav to " + $j("#turn_nav_" + turn_id))
}

function keep_current_turn_displayed(){
	display_turn(diplayed_turn);
}

function show_message(message){
	$('light').style.display='block';
	$('fade').style.display='block';
	$('message_content').innerHTML=message;
}

function hide_message(){
	$('light').style.display='none';
	$('fade').style.display='none';
	$('message_content').innerHTML="";
}

function change_message(message){
	$('message_content').innerHTML=message;	
}

function addLoadEvent(func) {
    /** This code no longer works as Window.onload is no longer supported
	var oldonload = window.onload;
    if (typeof window.onload != 'function') {
      window.onload = func;
    } else {
      window.onload = function() {
        oldonload();
        func();
      }
    }**/
	if (window.addEventListener)
    window.addEventListener("load", func, false);
  else if (window.attachEvent)
    window.attachEvent("onload", func);
  else { // fallback
    var old = window.onload;
    window.onload = function() {
      if (old) old();
      func();
    };
  }
	
	
}


