class ApplicationController < ActionController::Base
  protect_from_forgery
  helper :all # include all helpers, all the time
  #session :session_key => '_StrikeComRuby_session_id'

  include AuthenticatedSystem
  
  skip_before_filter :verify_authenticity_token # added to disable CSRF token authenticity check

  ActiveScaffold.set_defaults do |config| 
    config.ignore_columns.add [:created_at, :updated_at, :lock_version]
  end


  # See ActionController::RequestForgeryProtection for details
  # Uncomment the :secret if you're not using the cookie session store
  #protect_from_forgery # :secret => 'e5a0dcef9cda11afc76619cbdfa6ebb5'
  
  # See ActionController::Base for details 
  # Uncomment this to filter the contents of submitted sensitive data parameters
  # from your application log (in this case, all fields with names like "password"). 
  # filter_parameter_logging :password
  
  #automatically redirect to the list of current games for this player
  def index
    redirect_to :controller => 'game', :action => 'list'    
  end
  
 
  private
    #get the player from the current user, if there is one
    def load_player
      @player = current_user.is_owner_of_what[0]
      @game = @player.game if (@player)
    end
    
end
