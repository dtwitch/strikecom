class Admin::PlayerController < ApplicationController
  before_filter :login_required
  skip_before_filter :verify_authenticity_token # added to disable CSRF token authenticity check
  #permit "admin"
  layout "admin"
  active_scaffold do |config|
        config.columns = [:name, :assets, :committed_turns]
        config.columns = [:name, :assets]
        config.update.columns = [:name, :assets]
  end
end