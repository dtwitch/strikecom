# == Schema Information
# Schema version: 21000101000000
#
# Table name: assets
#
#  id             :integer(11)     not null, primary key
#  name           :string(255)     
#  width          :integer(11)     
#  height         :integer(11)     
#  game_id        :integer(11)     
#  asset_image_id :integer(11)     
#  prob_set_id    :integer(11)     
#  created_at     :datetime        
#  updated_at     :datetime        
#


class Admin::AssetController < ApplicationController
  before_filter :login_required
  skip_before_filter :verify_authenticity_token # added to disable CSRF token authenticity check
  #permit "admin"
  layout "admin"
  active_scaffold do |config|
    config.columns = [:name, :width, :height, :asset_image, :prob_set, :asset_instances]
  end
end
