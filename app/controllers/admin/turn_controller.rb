# == Schema Information
# Schema version: 21000101000000
#
# Table name: turns
#
#  id                        :integer(11)     not null, primary key
#  game_id                   :integer(11)     
#  name                      :string(255)     
#  order_num                 :integer(11)     
#  evaluated                 :boolean(1)      
#  copy_assets_from_previous :boolean(1)      
#  started_at                :datetime        
#  finished_at               :datetime        
#  created_at                :datetime        
#  updated_at                :datetime        
#

class Admin::TurnController < ApplicationController
  before_filter :login_required
  skip_before_filter :verify_authenticity_token # added to disable CSRF token authenticity check
  #permit "admin"
  layout "admin"
  active_scaffold do |config|
        config.columns = [:name, :order_num, :evaluated, :copy_assets_from_previous, :shared_visual, :started_at, :finished_at, :committed_players, :asset_instances]
        config.update.columns = [:name, :order_num, :copy_assets_from_previous, :shared_visual, :evaluated]
        config.create.columns = [:name, :order_num, :copy_assets_from_previous, :shared_visual, :evaluated]
        config.list.sorting = {:order_num => :asc }
  end
end