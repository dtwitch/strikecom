class Admin::UserController < ApplicationController
  before_filter :login_required
  skip_before_filter :verify_authenticity_token # added to disable CSRF token authenticity check
  #permit "admin" #TODO: authorization not working
  layout "admin"    
  active_scaffold :user do |config|
	config.label = "StrikeCom Games Users" #Added to remove the but of to_i
    config.columns.exclude :salt, :crypted_password, :remember_token, :remember_token_expires_at
  end    
end