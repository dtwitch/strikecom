# == Schema Information
# Schema version: 21000101000000
#
# Table name: games
#
#  id                 :integer(11)     not null, primary key
#  name               :string(255)     
#  description        :text            
#  template           :boolean(1)      
#  boardwidth         :integer(11)     
#  boardheight        :integer(11)     
#  boardimage         :string(255)     
#  imagewidth         :integer(11)     
#  imageheight        :integer(11)     
#  game_shared_visual :boolean(1)      
#  current_turn_id    :integer(11)     
#  default_terrain_id :integer(11)     
#  score              :decimal(6, 4)   
#  started_at         :datetime        
#  finished_at        :datetime        
#  created_at         :datetime        
#  updated_at         :datetime

#require "#{Rails.root}/vendor/plugins/zippy/lib/zippy.rb"
    
class Admin::GameController < ApplicationController
  before_filter :login_required
  skip_before_filter :verify_authenticity_token # added to disable CSRF token authenticity check
  #permit "admin" #TODO: authorization is not working
  layout "admin"

  active_scaffold :game do |config|
	config.label = "StrikeCom Games"
    config.columns = [:name, :template, :players, :turns, :targets, :assets, :score, :boardimage, :boardwidth, :boardheight, :imagewidth, :imageheight, :current_turn, :started_at, :finished_at, :show_pre_score]
    config.show.columns = [:name, :template, :players, :turns, :targets, :assets, :score, :boardimage, :boardwidth, :boardheight, :imagewidth, :imageheight, :current_turn, :started_at, :finished_at, :show_pre_score]
    config.update.columns = [:name, :template, :score, :boardimage, :boardwidth, :boardheight, :imagewidth, :imageheight, :current_turn, :show_pre_score]
    config.list.columns = [:name, :template, :score, :started_at, :finished_at, :show_pre_score, :players, :turns, :assets, :targets]  
	#columns[:name].set_link('nested', :parameters => {:associations => "players turns targets"})
	#columns[:name].set_link('nested', :parameters => {:associations => :name})
    config.action_links.add 'clone', 
                            :label => 'Clone', 
                            :action => 'deep_clone', 
                            :type => :member, 
                            :crud_type => :create,
                            :page => true,
                            :inline => true,
                            :position => :after
    config.action_links.add 'create_many', 
                            :label => 'Create Games',
                            :description => 'Create a bunch of games based on this template',
                            :action => 'new_many', 
                            :type => :member, 
                            :crud_type => :create, 
                            :popup => true
    config.action_links.add 'manage_users', 
                            :label => 'Manage Users', 
                            :action => 'manage_users', 
                            :type => :member, 
                            :crud_type => :update, 
                            :popup => true
    config.action_links.add 'delete_many', 
                            :label => 'Delete Many', 
                            :action => 'delete_many', 
                            :type => :table, 
                            :crud_type => :delete, 
                            :popup => true
    config.action_links.add 'get_chat', 
                            :label => 'Get chat', 
                            :action => 'chat', 
                            :type => :member, 
                            :crud_type => :read,
                            :position => :after,
                            :popup => true
    config.action_links.add 'get_all_chats',
                            :label => 'Get all started chats', 
                            :action => 'chats', 
                            :type => :collection, 
                            :crud_type => :read,
                            :popup => true
    config.action_links.add 'get_all_chats_by_player',
                            :label => 'Get all started chats by player', 
                            :action => 'chats', 
                            :parameters => {:by_player => 'true'},
                            :type => :collection, 
                            :crud_type => :read,
                            :popup => true         
  end

  #This is the main admin page for the game
  #def index
    #flash.keep
	#render :action => :list_templates
    #redirect_to :action => 'list_templates'
  #end
  
  def list_games
    #show only the games
  end
  
  def list_templates
    #show only the templates
  end
  
  #Returns the chat as a csv for the given game
  def chat
    #return the chat for this game
    game = Game.find(params[:id])
    respond_to do |format|
      format.csv do

      # send it to the browsah
      send_data game.to_csv,
                :type => 'text/csv; charset=iso-8859-1; header=present',
                :disposition => "attachment; filename=#{game.name}.csv"
     end
   end
  end
 
  #returns the chats for all games with chats as csv files in a zip file
  #view: chats.zip.zipper
  def chats
    by_player = params[:by_player]
    @games = Game.find(:all, :joins => "INNER JOIN players on players.game_id = games.id INNER JOIN chat_items on chat_items.player_id = players.id", :select => "games.*, count(chat_items.id) ci_count", :group => "players.game_id HAVING ci_count > 0")  
      z = Zippy.new do |zip|
        zip['readme.txt'] = "chats of games downloaded #{Time.now}"
        z = Proc.new { |game, player| zip["games/#{game.name}_#{player.name if player}_#{game.started_at.strftime('%A-%m-%d-%Y')}.csv"] = game.to_csv(player) }
        @games.each do |game|
         if(by_player)
            game.players.each do |player|
                z.call(game, player)
           end
         else
           z.call(game, nil)
         end
       end
     end
      # send it to the browsah
      send_data z.data,
              :type => 'application/zip',
              :disposition => "attachment; filename=chats.zip"      
  end

  #create one just like this
  #does not clone chat_items.  Best if used with template => true
  def deep_clone
    template = Game.find(params[:id])
    create_game(template, template.name)
    redirect_to :action => 'list'
	#render :action => :list
  end
  
  
  
  #Create a bunch of games
  def create_many
    template = Game.find(params[:id])
    params[:start].upto(params[:finish]) do |num|
      create_game_and_users(template, params[:name] + num.to_s, num)
    end
    redirect_to :action => 'list_templates'  #show the list when the games are done being created
	#render :action => :list_templates
  end
  
  def new_many
   @game_template = Game.find(params[:id])
  end
 
  def delete_many
    @games = Game.find(:all, :conditions => {:template => false})
  end
  
  def delete_many_action
    Game.destroy(params[:game].values.flatten.collect {|id| id.to_i })
    redirect_to :action => 'list_games'
	#render :action => :list_games
  end
  
  def delete_many_by_prefix
    Game.destroy_all "name LIKE '#{params[:game_prefix]}%'"
    redirect_to :action => 'list_games'
	#render :action => :list_games
  end
  
  def manage_users
    @game = Game.find(params[:id])
  end
  
  #Update the associated users
  def update_users
    begin
    @game = Game.find(params[:id])
    @player = Player.find(params[:player_id])
    user = User.find(params[:User][:id]) 
    user.has_role 'owner', @player
    rescue
      flash[:notice] = 'User was not successfully added.'
      raise
    else
      flash[:notice] = 'User was successfully added.'
    end
    redirect_to :action => 'manage_users', :id => @game
	#render :action => :manage_users, :id => @game
  end
  
  #delete a user from a game
  def delete_user
    begin
      @game = Game.find(params[:id])
      @player = Player.find(params[:player_id])
      @user = User.find(params[:user_id])
      @user.has_no_role 'owner', @player
    rescue
      flash[:notice] = 'User was not successfully deleted.'
      raise
    else
      flash[:notice] = 'User was successfully deleted.'
    end
    redirect_to :action => 'manage_users', :id => @game
	#render :action => :manage_users, :id => @game
  end

  protected 
      def create_game(template, name)
      flash[:notice] ||= ""
      begin
        new_game = Game.create_game(template, name)
        flash[:notice] << "Created #{name} by cloning #{template.name} <br/>"
        return new_game
      rescue
        flash[:notice] << "Could not #{name} by cloning #{template.name} <br/>"
        raise
      end
    end
  
    def create_game_and_users(template, name, user_suffix)
      flash[:notice] ||= ""
      begin
        game = Game.create_game_and_users(template, name, user_suffix)
        flash[:notice] << "Created game #{name} from #{template.name} and associated users <br/>"
        return game
      rescue
        game = Game.find_by_name(name)
        if game
          flash[:notice] << "Created game #{name} from #{template.name}, but could not create associated users with suffix #{user_suffix} <br/>"
        else
          flash[:notice] << "Could not create game #{name} from #{template.name} along with users with suffix #{user_suffix} <br/>"
        end
        raise
      end #rescue            
    end
  
end
