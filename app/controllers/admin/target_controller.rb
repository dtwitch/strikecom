class Admin::TargetController < ApplicationController
  before_filter :login_required
  skip_before_filter :verify_authenticity_token # added to disable CSRF token authenticity check
  #permit "admin"
  layout "admin"
  active_scaffold
end
