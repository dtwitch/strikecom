class ChatController < ApplicationController

before_filter :login_required
before_filter :load_player
skip_before_filter :verify_authenticity_token # added to disable CSRF token authenticity check

  #Add a comment to the chat
  def say
  	@player = Player.find(@player, :include => [:committed_turns, :game])
  	@game = @player.game
  	
    @chat_item = ChatItem.create(:player_id => @player.id, :content => params[:chat_item][:content])
  	
  	logger.info @player.name
  	logger.info @chat_item.content
  	
  	respond_to do |format|
  	  format.js
  	  format.html
  	end
  end
end
