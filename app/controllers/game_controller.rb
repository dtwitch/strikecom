require 'logger'
class GameController < ApplicationController
  before_filter :login_required
  before_filter :load_player
  @turn_shared_visual = true
  skip_before_filter :verify_authenticity_token #added to disable CSRF token authenticity check
  
  #default action... goes to list
  def index
    redirect_to :action => 'list'
  end
  
  #shows all of the games accessible by this user
  def list
    if current_user.has_role? "admin"
      redirect_to :controller => "admin/game", :action => "list" 
      @games = Game.find :all #for test, :joins => "LEFT JOIN players on players.game_id = games.id  JOIN chat_items on chat_items.player_id = players.id",:include => :players, :conditions => ["players.id = ?", @player.id] 
    else
		  @game = @player.game
      if @game == nil
        flash[:notice] = 'You are not signed up for a game'
      else 
        redirect_to :action => 'play', :id => @game.id
      end 
    end
  end  
  
  #play the game.  Actions during play are done through AJAX
  def play
    #if there is no game to play, then go to list
    if !params[:id]
      redirect_to :action => 'list'
    end
    #get rid of all events for this player since they will be refreshed
    Event.dequeue(@player.id)
    #@game = Game.find(params[:id], :include => [:chat_items, {:current_turn => [:asset_instances, :committed_players]}])
	@game = Game.find(params[:id]);
    @player = Player.find(@player, :include => :committed_turns)
    #get all of the instances in this turn
    @turn_asset_instances = {}
    @game.finished_and_current_turns.each do |turn|
      @turn_asset_instances[turn] = turn.board_asset_instances(@player)    
    end 
    
    begin_game(@game) unless @game.started_at #start the game if not already started
    
    @chat_items = @game.chat_items.find(:all, :include => :player)

    #cell dimensions are needed by the views
    calculate_cell_dimensions(@game)
    
    #get the turn to display, if any
    if(params[:displayturn])
      @display_turn = Turn.find(params[:displayturn]) 
    else
      @display_turn = @game.current_turn
    end
  end
  
  #Move asset from one place to another
  def move
    x = params[:x].to_i
    y = params[:y].to_i
    @game = Game.find(@player.game, :include => :current_turn)
    @player = Player.find(@player, :include => :committed_turns)
    #get the instances to remove
    instances_to_remove = AssetInstance.find(:all, :conditions => {:asset_id => params[:asset_id], :turn_id => @game.current_turn})

    #---calculate positions of new instances---
    #first get the asset that's being moved
    asset = Asset.find(params[:asset_id])
    #reduce x (no futher than 1) the amount that the asset would run off the board
    startx = [x - [((x + asset.width - 1) - @game.boardwidth),0].max,1].max  
    starty = [y - [((y + asset.height - 1) - @game.boardheight),0].max,1].max
    instances_to_add = []
    starty.upto(starty + asset.height - 1) do |vert|
      startx.upto(startx + asset.width - 1) do |horiz|
        instances_to_add << AssetInstance.new(:x => horiz, :y => vert, :asset => asset, :turn => @game.current_turn)
      end
    end
    
    #update the database
    AssetInstance.transaction do
      instances_to_remove.each {|instance| instance.destroy }
      instances_to_add.each{|instance| instance.save!}
    end
    
    #determine the cells where replacing will be needed
    xy_pairs = {}
    (instances_to_remove + instances_to_add).each do |instance|
      xy_pairs[instance.x.to_s + ',' + instance.y.to_s] = {:x => instance.x, :y => instance.y}   
    end
  
    calculate_cell_dimensions(@game)

    #get the coordinates for the off-board assets and delete them
    @off_board_coordinates = xy_pairs['0,0']
    xy_pairs.delete('0,0')

    #send to the players
    respond_to do |format|
      format.js
      format.html
    end    
  end
  
  #Commit a player for the turn
  def commit	
  Rails.logger.info "inside commit method"
    @game = Game.find(@player.game, :include => [:current_turn]) 
    @player = Player.find(@player, :include => [:committed_turns])
    if @player.committed?(@game.current_turn)	
      render :nothing => true
    else
      #hang on to the old current_turn
      @old_current_turn = @game.current_turn
      @evaluated = false
      begin
        #only commit and evaluate together
        Game.transaction do
          #commit the player for this turn
          @player.commit_current_turn
          #evaluate the board for this turn (if needed) returns true if evaluated
          #current turn might be changed here
          @evaluated = @game.current_turn.evaluate
        end
      rescue
        raise
      else
        #reload the game since things are likely to have changed
        @game = Game.find(@game, :include => [:current_turn, {:players => :committed_turns}])           
        calculate_cell_dimensions(@game)
        #if the turn is completely finished, have all of the players in the reload the browser, calling "play"
        #have it display the old turn, though, to see the results of the evaluation.

      end
    end
  end
  
  def score
    @game = Game.find(params[:id])
    @score_components = @game.score_components
  end

  def pre_score
    @game = Game.find(params[:id])
  end

  private
    
    def calculate_cell_dimensions(game)
      #calculate the game_board cell sizes
      @cell_width = (game.imagewidth / game.boardwidth) * 0.8
      @cell_height = (game.imageheight / game.boardheight) * 0.8
    end
    
    #set the game to begin
    def begin_game(game)
        game.started_at = Time.now
        game.save
    end  
    
    def pre_score_message()
      return ("<p>Thank you for playing StrikeCom.</p>
      <p>Please fill out the questionnaire before continuing.  When you are finished, Click OK to continue</p>
      <button type=\"button\" onclick=\"show_score('Game score here!')\">OK</button>")
    end
end
