// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults

var diplayed_turn = 0;

function display_turn(turn_id){
	diplayed_turn = turn_id;
	//remove the shown_turn class from all other turns
	$$('.turn').each(function(turn){
		turn.removeClassName('shown_turn');
	});
	//set the turn navs of other turns to not shown
	$$('.turn_nav').each(function(turn){
		turn.removeClassName('shown_turn_nav');
	});
	//set display of selected turn and its turn_nav to shown
	$('turn_' + turn_id).addClassName('shown_turn');
	$('turn_nav_' + turn_id).addClassName('shown_turn_nav');
}

function keep_current_turn_displayed(){
	display_turn(diplayed_turn);
}

function show_message(message){
	$('light').style.display='block';
	$('fade').style.display='block';
	$('message_content').innerHTML=message;
}

function hide_message(){
	$('light').style.display='none';
	$('fade').style.display='none';
	$('message_content').innerHTML="";
}

function change_message(message){
	$('message_content').innerHTML=message;	
}

function addLoadEvent(func) {
    var oldonload = window.onload;
    if (typeof window.onload != 'function') {
      window.onload = func;
    } else {
      window.onload = function() {
        oldonload();
        func();
      }
    }
}


