//included during gameplay only

var pe;
var INTERVAL = 3;

function startUpdaterURL(url){
	pe = new PeriodicalExecuter(function() {new Ajax.Request(url, {asynchronous:true, evalScripts:true})}, INTERVAL)
}

function stopUpdater(){
	pe.stop();
}

function cursor_wait() {
  document.body.style.cursor = 'wait';
}

function cursor_clear() {
  document.body.style.cursor = 'default';
}

function scrollDivDown(div){
	var s = $(div) 
	if (s) {
  	s.scrollTop = s.scrollHeight - s.clientHeight;
  }  
}

addLoadEvent(scrollDivDown("chat_items"))